<?php
 
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    
    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
        ];
    }

    /**
     * @param int count le nombre à controler
     * @param string singular le singuler
     * @param plural optionnel
     * 
     * @return string
     */
    public function pluralize(int $count, string $singular, ?string $plural=null) : string
    {
        /* Si pas de pluriel, on le contruit*/
        $plural ??=  $singular . 's';
        /* On regarde selon le nombre ce que l'on doit retourner*/
        $res = $count> 1 ? $plural : $singular;

            return $count .' '. $res;
        
    }
}
