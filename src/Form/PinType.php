<?php

namespace App\Form;

use App\Entity\Pin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
        $imageFileConstraints = [];

        $isEdit = $options['method']=='PUT';

        if ($isEdit)*/
        $builder
        ->add('imageFile', VichImageType::class, [
            'label' => 'Image (Png or Jpg)',
            'required' => false,
            'allow_delete' => true,
            //'delete_label' => '...',
            //'download_label' => '...',
            'download_uri' => false,
            //'image_uri' => true,
            'imagine_pattern' => 'squared_thumbnail_small',
            //'asset_helper' => true,
            //'constraints' => [
//                new Image(['maxSize' =>'1M'])
            //]
        ])
        ->add('title',TextType::class,['label'=>'Titre', 'attr' => ['autofocus' => true]])
        ->add('description',TextareaType::class, ['label'=>'Desc', 'attr' => ['cols' => '60', 'rows'=>'10']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pin::class,
        ]);
    }
}
