<?php

namespace App\Controller;

use App\Entity\Pin;
use App\Form\PinType;
use App\Repository\PinRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
// use Symfony\Component\Routing\Annotation\Route;

class PinsController extends AbstractController
{


    /* @Route("/pins", name="pins")
    /**
     * @Route("/")
     */
    public function index(PinRepository $repo): Response
    {

        $pins = $repo->findBy([], ['createdAt' => 'DESC']);

        return $this->render('pins/index.html.twig', [
            'controller_name' => 'PinsController',
            'pins' => $pins,
        ]);
        /*return $this->json([
            'message' => 'Welcome to your new controller',
            'path' => 'src/Controller/PinsController.php',
            ]);*/
        //return new Response('Hello World');
    }

    /**
     * @ParamConverter("pin")
     */
    public function show(Pin $pin /*PinRepository $repo,int $id*/): Response
    {
        //     $pin=$repo->find($id);
        //     if ( ! $pin)
        //     {
        // throw $this->createNotFoundException("Pin #". $id ." not found");
        //     }
        return $this->render('pins/show.html.twig', [
            'controller_name' => 'PinsController',
            'pin' => $pin,
        ]);
    }

    /**
     * @ParamConverter("pin")
     * IsGranted ("PIN_MANAGE", subject="pin")
     */
    public function delete(Request $request, EntityManagerInterface $em, Pin $pin): Response
    {
        if ($this->isCsrfTokenValid('pin_deletion_' . $pin->getId(), $request->request->get('csrf_token'))) {
            $em->remove($pin);
            $em->flush();
            //permet d'afficher un message 
            $this->addFlash('info', 'Pin successfully deleted!');
        }
        return $this->redirectToRoute('app_home');
    }
    /**
     * @ParamConverter("pin")
     * @Security("is_granted('PIN_MANAGE', pin)")
     
     */
    public function edit(Request $request, EntityManagerInterface $em, Pin $pin): Response
    {

        // $this->denyAccessUnlessGranted('PIN_EDIT', $pin);
        $form = $this->createForm(PinType::class, $pin, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message 
            $this->addFlash('success', 'Pin successfully updated!');
            return $this->redirectToRoute('app_pins_show', ['id' => $pin->getId()]);
        }
        return $this->render('pins/edit.html.twig', [
            'controller_name' => 'PinsController',
            'pin' => $pin,
            'monForm' => $form->createView(),
        ]);
    }


    /**
     * @Route("/pin/create", name="app_pins_create" ,methods="GET|POST")
     * @Security("is_granted('PIN_CREATE')")
     */
    public function create(Request $request, EntityManagerInterface $em, UserRepository $userRepo): Response
    {

        // if (!$this->getUser()) {
        //     throw $this->createAccessDeniedException('Tentative de création sans être connecté');
        //     //$this->addFlash('error', 'Not Connected!');
        //     //return $this->redirectToRoute('app_login');
        // }
        // if (!$this->getUser()->isVerified) {
        //     throw $this->createAccessDeniedException('You need to have a verified account');
        //     //$this->addFlash('error', 'Not Connected!');
        //     //return $this->redirectToRoute('app_login');
        // }

        $pin = new Pin;

        $form = $this->createForm(PinType::class, $pin);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pin->setUser($this->getUser());
            $em->persist($pin);
            $em->flush();
            //permet d'afficher un message 
            $this->addFlash('success', 'Pin successfully created!');
            return $this->redirectToRoute('app_pins_show', ['id' => $pin->getId()]);
        }
        return $this->render('pins/create.html.twig', [
            'controller_name' => 'PinsController',
            'monForm' => $form->createView(),
        ]);
    }
}
