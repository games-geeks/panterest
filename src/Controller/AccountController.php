<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManager;
use App\Form\ChangePasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/account")
 * @IsGranted("ROLE_USER")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("", name="app_account",methods="GET")
     */
    public function show(): Response
    {
        //$this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('account/show.html.twig');
    }

    /**
     * @Route("/edit", name="account_edit",methods="GET|POST")
     * @IsGranted ("IS_AUTHENTICATED_FULLY")
     */
    public function edit(Request $request, EntityManagerInterface $em, FlashyNotifier $flashy): Response
    {
        //pour git
        $user = $this->getUser();
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $flashy->success('Event created!');
            //permet d'afficher un message 
            $this->addFlash('success', 'User successfully updated!');
            return $this->redirectToRoute('app_account', ['id' => $user->getId()]);
        }

        return $this->render('account/edit.html.twig', [
            'monForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/change-password", name="account_change_password",methods="GET|POST")
     * @IsGranted ("IS_AUTHENTICATED_FULLY")
     */
    public function changePassord(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordFormType::class, null, [
            'current_password_is_required' => true
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword($passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));
            $em->flush();
            //permet d'afficher un message 
            $this->addFlash('success', 'Password successfully updated!');
            return $this->redirectToRoute('app_account', ['id' => $user->getId()]);
        }
        return $this->render(
            'account/change_password.html.twig',
            [
                'monForm' => $form->createView()
            ]
        );
    }
}
