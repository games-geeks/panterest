<h1 align="center">Welcome to Panterest 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: UNLICENSED" src="https://img.shields.io/badge/License-UNLICENSED-yellow.svg" />
  </a>
  <a href="https://twitter.com/gamesngeeks" target="_blank">
    <img alt="Twitter: gamesngeeks" src="https://img.shields.io/twitter/follow/gamesngeeks.svg?style=social" />
  </a>
</p>

> Clone de Pinterest dans le cadre de la découverte de Symfony

## Install

```sh
npm install
```

## Author

👤 **Denny**

* Website: www.games-geeks.fr
* Twitter: [@gamesngeeks](https://twitter.com/gamesngeeks)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_